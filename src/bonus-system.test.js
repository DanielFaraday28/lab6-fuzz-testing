import calculateBonuses from "./bonus-system.js";
const assert = require("assert");


function getMultiplierByProgram(program) {
    return program === "Standard" ? 0.05 :
            program === "Premium" ? 0.1 :
            program === "Diamond" ? 0.2 : 0;
}


function getBonusByAmount(amount) {
    return amount < 10000 ? 1 :
            amount < 50000 ? 1.5 :
            amount < 100000 ? 2 : 2.5;
}


describe('Bonus system tests', () => {
    let app;
    console.log("Tests started");


    test('Valid sum test',  (done) => {
        let program = "Standard";
        let amount = 2000;
        assert.equal(calculateBonuses(program, amount), getMultiplierByProgram(program) * getBonusByAmount(amount));
        done();
    });

    test('Valid sum test',  (done) => {
        let program = "Standard";
        let amount = 10000;
        assert.equal(calculateBonuses(program, amount), getMultiplierByProgram(program) * getBonusByAmount(amount));
        done();
    });

    test('Valid sum test',  (done) => {
        let program = "Standard";
        let amount = 50000;
        assert.equal(calculateBonuses(program, amount), getMultiplierByProgram(program) * getBonusByAmount(amount));
        done();
    });

    test('Valid sum test',  (done) => {
        let program = "Standard";
        let amount = 100000;
        assert.equal(calculateBonuses(program, amount), getMultiplierByProgram(program) * getBonusByAmount(amount));
        done();
    });

    test('Valid sum test',  (done) => {
        let program = "Premium";
        let amount = 2000;
        assert.equal(calculateBonuses(program, amount), getMultiplierByProgram(program) * getBonusByAmount(amount));
        done();
    });

    test('Valid sum test',  (done) => {
        let program = "Premium";
        let amount = 10000;
        assert.equal(calculateBonuses(program, amount), getMultiplierByProgram(program) * getBonusByAmount(amount));
        done();
    });

    test('Valid sum test',  (done) => {
        let program = "Premium";
        let amount = 50000;
        assert.equal(calculateBonuses(program, amount), getMultiplierByProgram(program) * getBonusByAmount(amount));
        done();
    });

    test('Valid sum test',  (done) => {
        let program = "Premium";
        let amount = 100000;
        assert.equal(calculateBonuses(program, amount), getMultiplierByProgram(program) * getBonusByAmount(amount));
        done();
    });

    test('Valid sum test',  (done) => {
        let program = "Diamond";
        let amount = 2000;
        assert.equal(calculateBonuses(program, amount), getMultiplierByProgram(program) * getBonusByAmount(amount));
        done();
    });

    test('Valid sum test',  (done) => {
        let program = "Diamond";
        let amount = 10000;
        assert.equal(calculateBonuses(program, amount), getMultiplierByProgram(program) * getBonusByAmount(amount));
        done();
    });

    test('Valid sum test',  (done) => {
        let program = "Diamond";
        let amount = 50000;
        assert.equal(calculateBonuses(program, amount), getMultiplierByProgram(program) * getBonusByAmount(amount));
        done();
    });

    test('Valid sum test',  (done) => {
        let program = "Diamond";
        let amount = 100000;
        assert.equal(calculateBonuses(program, amount), getMultiplierByProgram(program) * getBonusByAmount(amount));
        done();
    });

    test('Valid sum test',  (done) => {
        let program = "dummy";
        let amount = 2000;
        assert.equal(calculateBonuses(program, amount), getMultiplierByProgram(program) * getBonusByAmount(amount));
        done();
    });

    test('Valid sum test',  (done) => {
        let program = "dummy";
        let amount = 10000;
        assert.equal(calculateBonuses(program, amount), getMultiplierByProgram(program) * getBonusByAmount(amount));
        done();
    });

    test('Valid sum test',  (done) => {
        let program = "dummy";
        let amount = 50000;
        assert.equal(calculateBonuses(program, amount), getMultiplierByProgram(program) * getBonusByAmount(amount));
        done();
    });

    test('Valid sum test',  (done) => {
        let program = "dummy";
        let amount = 100000;
        assert.equal(calculateBonuses(program, amount), getMultiplierByProgram(program) * getBonusByAmount(amount));
        done();
    });


    console.log('Tests Finished');

});
